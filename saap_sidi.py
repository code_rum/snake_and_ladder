import random
from typing import Type


class Board:
    def __init__(self, board_end: int, snake_bites: dict) -> None:
        self.board_position = 0
        self.board_end = 100
        self.snake_bites = snake_bites

class PlayerMovement:
    def __init__(self, board: Type[Board]) -> None:
        self.board_position = board.board_position
        self.board_end = board.board_end
        self.snake_bites = board.snake_bites

    def move_forward(self, steps: int) -> str:
        future_position = self.board_position + steps
        if self.snake_bite_check(future_position): 
            self.board_position = self.snake_bites.get(future_position)
            return f"You've have been bitten by snake. Your new position is {self.board_position}"
        elif self.winner_check(future_position):
            self.board_position = future_position
            return "Congratulation!! You're the winner"
        elif self.board_outer_limit(future_position):
            required_dice_num = self.board_end - self.board_position
            return f"Sorry cannot move forward. You require {required_dice_num}"
        self.board_position = future_position
        return f"You are at {self.board_position}"

    def current_postion(self) -> int:
        return self.board_position

    def board_outer_limit(self, future_position: int) -> bool:
        return future_position > self.board_end

    def winner_check(self, future_position: int) -> bool:
        return future_position == self.board_end

    def snake_bite_check(self, future_position: int) -> bool:
        return future_position in self.snake_bites.keys()

class Dice:
    def __init__(self, dice_type: str) -> None:
        self.dice_type = dice_type

    def rolling(self) -> None:
        raise NonImplementedError

    def __str__(self):
        raise NonImplementedError

class CrookedDice(Dice):
    def rolling(self):
        steps = random.randint(1, 6)
        return steps

    def __str__(self):
        return f"{self.dice_type}"


class NormalDice(Dice):
    def rolling(self):
        while steps := random.randint(1, 6):
            if steps % 2 == 0:
                return steps

    def __str__(self):
        return f"{self.dice_type}"

class ChooseDice:
    def __init__(self, dice_factory: Type[Dice]) -> None:
        self.dice_factory = dice_factory

    def dice_selection(self, dice_type: str) -> Dice:
        dice = self.dice_factory(dice_type)
        print(f"You choose dice type {dice}")
        return dice

if __name__ == "__main__":
    dice_factory = ChooseDice(NormalDice)
    dice = dice_factory.dice_selection('Normal')
    board_limit = 100
    snake_bites = {14: 7}
    board = Board(board_limit, snake_bites)
    player_1 = PlayerMovement(board)
    print(f"Initial position {board.board_position}")
    count = 1
    while count <= 200:
        steps = dice.rolling()
        print(f"Shakuni mama. Dice rolled. {steps}")
        print(player_1.move_forward(steps))
        if player_1.current_postion() == player_1.board_end:
            break
        count += 1


